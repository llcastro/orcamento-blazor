using OrcamentoBlazor.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Client.Services
{
   public interface IClienteService
   {
      Task<ClienteModel> Create(ClienteModel clienteModel);

      Task<int> Delete(int id);

      Task<ClienteModel> GetById(int id);

      Task<int> Count(string search);

      Task<List<ClienteModel>> ListAll(int skip, int take, string orderBy, string direction, string search);

      Task<ClienteModel> Update(ClienteModel clienteModel);

      Task<List<ClienteModel>> FetchAll();
   }
}