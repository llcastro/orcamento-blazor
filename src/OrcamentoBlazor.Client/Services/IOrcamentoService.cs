using OrcamentoBlazor.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Client.Services
{
   public interface IOrcamentoService
   {
      Task<OrcamentoModel> Create(OrcamentoModel orcamentoModel);

      Task<int> Delete(int id);

      Task<OrcamentoModel> GetById(int id);

      Task<int> Count(string search);

      Task<List<OrcamentoModel>> ListAll(int skip, int take, string orderBy, string direction, string search);

      Task<OrcamentoModel> Update(OrcamentoModel orcamentoModel);
      Task<OrcamentoProdutoModel> AddProduto(OrcamentoProdutoModel orcamentoModel);
      Task<int> DeleteProduto(int id);
      Task<List<OrcamentoProdutoModel>> FetchAllProdutos(int orcamento);
   }
}