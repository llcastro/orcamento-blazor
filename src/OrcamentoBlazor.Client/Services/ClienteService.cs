using OrcamentoBlazor.Shared;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace OrcamentoBlazor.Client.Services
{
  public class ClienteService : IClienteService
  {
    private readonly HttpClient _httpClient;

    public ClienteService(HttpClient httpClient)
    {
      _httpClient = httpClient;
    }

    public async Task<ClienteModel> Create(ClienteModel clienteModel)
    {
      var result = await _httpClient.PostJsonAsync<ClienteModel>("api/cliente", clienteModel);

      return result;
    }

    public async Task<ClienteModel> GetById(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<ClienteModel>("api/cliente/getbyid", data);

      return result;
    }

    public async Task<int> Delete(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<int>("api/cliente/delete", data);

      return result;
    }
    
    public async Task<int> Count(string search)
    {
      var data = new ListParams()
      {
        search = search
      };

      var result = await _httpClient.PostJsonAsync<int>("api/cliente/count", data);

      return result;
    }
    
    public async Task<List<ClienteModel>> ListAll(int skip, int take, string orderBy, string direction="DESC", string search = "")
    {
      var data = new ListParams()
      {
        skip = skip,
        take = take,
        orderBy = orderBy,
        direction = direction,
        search = search
      };

      var result = await _httpClient.PostJsonAsync<List<ClienteModel>>("api/cliente/listAll", data);

      return result;
    }

    public async Task<ClienteModel> Update(ClienteModel clienteModel)
    {
      var result = await _httpClient.PostJsonAsync<ClienteModel>("api/cliente", clienteModel);

      return result;
    }

    public async Task<List<ClienteModel>> FetchAll()
    {
      var result = await _httpClient.PostJsonAsync<List<ClienteModel>>("api/cliente/fetchAll", new ListParams());

      return result;
    }
  }
}