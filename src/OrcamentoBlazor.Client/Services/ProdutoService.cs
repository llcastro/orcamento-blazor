using OrcamentoBlazor.Shared;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace OrcamentoBlazor.Client.Services
{
  public class ProdutoService : IProdutoService
  {
    private readonly HttpClient _httpClient;

    public ProdutoService(HttpClient httpClient)
    {
      _httpClient = httpClient;
    }

    public async Task<ProdutoResult> Create(ProdutoModel produtoModel)
    {
      var result = await _httpClient.PostJsonAsync<ProdutoResult>("api/produto", produtoModel);

      return result;
    }

    public async Task<ProdutoModel> GetById(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<ProdutoModel>("api/produto/getbyid", data);

      return result;
    }

    public async Task<int> Delete(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<int>("api/produto/delete", data);

      return result;
    }
    
    public async Task<int> Count(string search)
    {
      var data = new ListParams()
      {
        search = search
      };

      var result = await _httpClient.PostJsonAsync<int>("api/produto/count", data);

      return result;
    }
    
    public async Task<List<ProdutoModel>> ListAll(int skip, int take, string orderBy, string direction="DESC", string search = "")
    {
      var data = new ListParams()
      {
        skip = skip,
        take = take,
        orderBy = orderBy,
        direction = direction,
        search = search
      };

      var result = await _httpClient.PostJsonAsync<List<ProdutoModel>>("api/produto/listAll", data);

      return result;
    }

    public async Task<ProdutoModel> Update(ProdutoModel produtoModel)
    {
      var result = await _httpClient.PostJsonAsync<ProdutoModel>("api/produto", produtoModel);

      return result;
    }

    public async Task<List<ProdutoModel>> FetchAll()
    {
      var result = await _httpClient.PostJsonAsync<List<ProdutoModel>>("api/produto/fetchAll", new ListParams());

      return result;
    }
  }
}