using OrcamentoBlazor.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Client.Services
{
   public interface IProdutoService
   {
      Task<ProdutoResult> Create(ProdutoModel produtoModel);

      Task<int> Delete(int id);

      Task<ProdutoModel> GetById(int id);

      Task<int> Count(string search);

      Task<List<ProdutoModel>> ListAll(int skip, int take, string orderBy, string direction, string search);

      Task<ProdutoModel> Update(ProdutoModel produtoModel);

      Task<List<ProdutoModel>> FetchAll();
   }
}