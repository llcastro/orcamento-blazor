using OrcamentoBlazor.Shared;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace OrcamentoBlazor.Client.Services
{
  public class OrcamentoService : IOrcamentoService
  {
    private readonly HttpClient _httpClient;

    public OrcamentoService(HttpClient httpClient)
    {
      _httpClient = httpClient;
    }

    public async Task<OrcamentoModel> Create(OrcamentoModel orcamentoModel)
    {
      var result = await _httpClient.PostJsonAsync<OrcamentoModel>("api/orcamento", orcamentoModel);

      return result;
    }

    public async Task<OrcamentoModel> GetById(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<OrcamentoModel>("api/orcamento/getbyid", data);

      return result;
    }

    public async Task<int> Delete(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<int>("api/orcamento/delete", data);

      return result;
    }
    
    public async Task<int> Count(string search)
    {
      var data = new ListParams()
      {
        search = search
      };

      var result = await _httpClient.PostJsonAsync<int>("api/orcamento/count", data);

      return result;
    }
    
    public async Task<List<OrcamentoModel>> ListAll(int skip, int take, string orderBy, string direction="DESC", string search = "")
    {
      var data = new ListParams()
      {
        skip = skip,
        take = take,
        orderBy = orderBy,
        direction = direction,
        search = search
      };

      var result = await _httpClient.PostJsonAsync<List<OrcamentoModel>>("api/orcamento/listAll", data);

      return result;
    }

    public async Task<OrcamentoModel> Update(OrcamentoModel orcamentoModel)
    {
      var result = await _httpClient.PostJsonAsync<OrcamentoModel>("api/orcamento", orcamentoModel);

      return result;
    }

    public async Task<OrcamentoProdutoModel> AddProduto(OrcamentoProdutoModel orcamentoModel)
    {
      var result = await _httpClient.PostJsonAsync<OrcamentoProdutoModel>("api/orcamento/produto", orcamentoModel);

      return result;
    }

    public async Task<int> DeleteProduto(int id)
    {
      var data = new ListParams()
      {
        id = id
      };

      var result = await _httpClient.PostJsonAsync<int>("api/orcamento/deleteProduto", data);

      return result;
    }

    public async Task<List<OrcamentoProdutoModel>> FetchAllProdutos(int orcamento)
    {
      var data = new ListParams()
      {
        id = orcamento
      };

      var result = await _httpClient.PostJsonAsync<List<OrcamentoProdutoModel>>("api/orcamento/fetchAllProdutos", data);

      return result;
    }
  }
}