using OrcamentoBlazor.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server
{
   public interface IClienteServiceDapper
   {
      Task<int> Create(ClienteModel cliente);
      Task<int> Delete(int Id);
      Task<int> Count(string search);
      Task<int> Update(ClienteModel cliente);
      Task<ClienteModel> GetById(int Id);
      Task<List<ClienteModel>> ListAll(int skip, int take,
          string orderBy, string direction, string search);
      Task<List<ClienteModel>> FetchAll();
   }
}