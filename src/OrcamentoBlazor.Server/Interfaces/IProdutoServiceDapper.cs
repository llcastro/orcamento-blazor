using OrcamentoBlazor.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server
{
   public interface IProdutoServiceDapper
   {
      Task<int> Create(ProdutoModel produto);
      Task<int> Delete(int Id);
      Task<int> Count(string search);
      Task<int> Update(ProdutoModel produto);
      Task<ProdutoModel> GetById(int Id);
      Task<List<ProdutoModel>> ListAll(int skip, int take,
          string orderBy, string direction, string search);
      Task<List<ProdutoModel>> FetchAll();
   }
}