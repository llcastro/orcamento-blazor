using OrcamentoBlazor.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server
{
   public interface IOrcamentoServiceDapper
   {
      Task<int> Create(OrcamentoModel orcamento);
      Task<int> Delete(int Id);
      Task<int> Count(string search);
      Task<int> Update(OrcamentoModel orcamento);
      Task<OrcamentoModel> GetById(int Id);
      Task<List<OrcamentoModel>> ListAll(int skip, int take,
          string orderBy, string direction, string search);
      Task<int> CreateProduto(OrcamentoProdutoModel produto);
      Task<int> DeleteProduto(int id);
      Task<List<OrcamentoProdutoModel>> FetchAllProdutos(int orcamento);
   }
}