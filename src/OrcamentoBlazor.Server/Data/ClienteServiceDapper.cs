using OrcamentoBlazor.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server.Data
{
   public class ClienteServiceDapper : IClienteServiceDapper
  {
      private readonly IDapperService _dapperService;
      
      public ClienteServiceDapper(IDapperService dapperService)
      {
         this._dapperService = dapperService;
      }

      public Task<int> Create(ClienteModel cliente)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Nome", cliente.Nome, DbType.String);
         dbPara.Add("Telefone", cliente.Telefone, DbType.String);
         var clienteId = Task.FromResult(_dapperService.Insert<int>("[dbo].[prcInsAltCliente]", dbPara, commandType: CommandType.StoredProcedure));
         return clienteId;
      }
      
      public Task<ClienteModel> GetById(int id)
      {
         var cliente = Task.FromResult
            (_dapperService.Get<ClienteModel> 
            ($"select * from [cliente] where Id = {id}", null,
            commandType: CommandType.Text));
         return cliente;
      }
      
      public Task<int> Delete(int id)
      {
         var deletecliente = Task.FromResult
            (_dapperService.Execute
            ($"Delete [cliente] where Id = {id}", null,
            commandType: CommandType.Text));
         return deletecliente;
      }
      
      public Task<int> Count(string search)
      {
         var totcliente = Task.FromResult(_dapperService.Get<int>
            ($"select COUNT(*) from [cliente] WHERE Nome like '%{search}%'", null,
            commandType: CommandType.Text));
         return totcliente;
      }
      
      public Task<List<ClienteModel>> ListAll (int skip, int take, string orderBy, string direction="DESC", string search = "")
      {
         var clientes = Task.FromResult
            (_dapperService.GetAll<ClienteModel>
            ($"SELECT * FROM [cliente] WHERE Nome like '%{search}%' ORDER BY {orderBy} {direction} OFFSET {skip} ROWS FETCH NEXT {take} ROWS ONLY; ", null, commandType: CommandType.Text));
         return clientes;
      }
      
      public Task<int> Update(ClienteModel cliente)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Id", cliente.Id);
         dbPara.Add("Nome", cliente.Nome, DbType.String);
         dbPara.Add("Telefone", cliente.Telefone, DbType.String);
         var updateCliente = Task.FromResult
            (_dapperService.Update<int>("[dbo].[prcInsAltCliente]",
            dbPara, commandType: CommandType.StoredProcedure));
         return updateCliente;
      }

      public Task<List<ClienteModel>> FetchAll()
      {
         var bookAuthorNames = Task.FromResult(_dapperService.GetAll
             <ClienteModel>($"select * from [cliente] order by Nome; ", null,
             commandType: CommandType.Text));
         return bookAuthorNames;
      }
   }
}