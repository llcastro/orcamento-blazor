using OrcamentoBlazor.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server.Data
{
   public class OrcamentoServiceDapper : IOrcamentoServiceDapper
  {
      private readonly IDapperService _dapperService;
      
      public OrcamentoServiceDapper(IDapperService dapperService)
      {
         this._dapperService = dapperService;
      }

      public Task<int> Create(OrcamentoModel orcamento)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Cliente", orcamento.Cliente, DbType.Int32);
         // dbPara.Add("Produto", orcamento.Produto.Id, DbType.Int32);
         var orcamentoId = Task.FromResult(_dapperService.Insert<int>("[dbo].[prcInsAltOrcamento]", dbPara, commandType: CommandType.StoredProcedure));
         return orcamentoId;
      }
      
      public Task<OrcamentoModel> GetById(int id)
      {
         var orcamento = Task.FromResult
            (_dapperService.Get<OrcamentoModel> 
            ($"select * from [orcamento] where Id = {id}", null,
            commandType: CommandType.Text));
         return orcamento;
      }
      
      public Task<int> Delete(int id)
      {
         var deleteorcamento = Task.FromResult
            (_dapperService.Execute
            ($"Delete [orcamento] where Id = {id}", null,
            commandType: CommandType.Text));
         return deleteorcamento;
      }
      
      public Task<int> Count(string search)
      {
         var totorcamento = Task.FromResult(_dapperService.Get<int>
            ($"select COUNT(*) from [orcamento] o inner join cliente c on o.Cliente = c.Id WHERE c.Nome like '%{search}%'", null,
            commandType: CommandType.Text));
         return totorcamento;
      }
      
      public Task<List<OrcamentoModel>> ListAll(int skip, int take, string orderBy, string direction="DESC", string search = "")
      {
         var orcamentos = Task.FromResult
            (_dapperService.GetAll<OrcamentoModel>
            ($"SELECT o.*, c.Nome as ClienteNome, (select p.Nome + '; ' as [text()] from produto p inner join dbo.orcamento_produtos op on p.Id = op.Produto where op.Orcamento = o.Id order by p.Nome for xml path ('')) as ProdutosList FROM [orcamento] o inner join cliente c on o.Cliente = c.Id WHERE c.Nome like '%{search}%' ORDER BY o.{orderBy} {direction} OFFSET {skip} ROWS FETCH NEXT {take} ROWS ONLY; ", null, commandType: CommandType.Text));
         return orcamentos;
      }

      public Task<int> Update(OrcamentoModel orcamento)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Id", orcamento.Id);
         dbPara.Add("Cliente", orcamento.Cliente, DbType.Int32);
         // dbPara.Add("Produto", orcamento.Produto.Id, DbType.Int32);
         var updateOrcamento = Task.FromResult(_dapperService.Update<int>("[dbo].[prcInsAltOrcamento]", dbPara, commandType: CommandType.StoredProcedure));
         return updateOrcamento;
      }

      public Task<int> CreateProduto(OrcamentoProdutoModel produto)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Orcamento", produto.Orcamento, DbType.Double);
         dbPara.Add("Produto", produto.Produto, DbType.Double);
         var produtoId = Task.FromResult(_dapperService.Insert<int>("[dbo].[prcInsAltOrcamentoProduto]", dbPara, commandType: CommandType.StoredProcedure));
         return produtoId;
      }

      public Task<int> DeleteProduto(int id)
      {
         var deleteorcamento = Task.FromResult
            (_dapperService.Execute
            ($"Delete [orcamento_produtos] where Id = {id}", null,
            commandType: CommandType.Text));
         return deleteorcamento;
      }

      public Task<List<OrcamentoProdutoModel>> FetchAllProdutos(int orcamento)
      {
         var orcamentos = Task.FromResult
            (_dapperService.GetAll<OrcamentoProdutoModel>
            ($"select op.*, p.Nome as ProdutoNome from orcamento_produtos op inner join produto p on op.Produto = p.Id where op.Orcamento = {orcamento}; ", null, commandType: CommandType.Text));
         return orcamentos;
      }
   }
}