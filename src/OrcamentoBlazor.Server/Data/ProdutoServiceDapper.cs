using OrcamentoBlazor.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server.Data
{
   public class ProdutoServiceDapper : IProdutoServiceDapper
  {
      private readonly IDapperService _dapperService;
      
      public ProdutoServiceDapper(IDapperService dapperService)
      {
         this._dapperService = dapperService;
      }

      public Task<int> Create(ProdutoModel produto)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Nome", produto.Nome, DbType.String);
         dbPara.Add("Preco", produto.Preco, DbType.Double);
         var produtoId = Task.FromResult(_dapperService.Insert<int>("[dbo].[prcInsAltProduto]", dbPara, commandType: CommandType.StoredProcedure));
         return produtoId;
      }
      
      public Task<ProdutoModel> GetById(int id)
      {
         var produto = Task.FromResult
            (_dapperService.Get<ProdutoModel> 
            ($"select * from [produto] where Id = {id}", null,
            commandType: CommandType.Text));
         return produto;
      }
      
      public Task<int> Delete(int id)
      {
         var deleteProduto = Task.FromResult
            (_dapperService.Execute
            ($"Delete [produto] where Id = {id}", null,
            commandType: CommandType.Text));
         return deleteProduto;
      }
      
      public Task<int> Count(string search)
      {
         var totProduto = Task.FromResult(_dapperService.Get<int>
            ($"select COUNT(*) from [Produto] WHERE Nome like '%{search}%'", null,
            commandType: CommandType.Text));
         return totProduto;
      }
      
      public Task<List<ProdutoModel>> ListAll (int skip, int take, string orderBy, string direction="DESC", string search = "")
      {
         var produtos = Task.FromResult
            (_dapperService.GetAll<ProdutoModel>
            ($"SELECT * FROM [Produto] WHERE Nome like '%{search}%' ORDER BY {orderBy} {direction} OFFSET {skip} ROWS FETCH NEXT {take} ROWS ONLY; ", null, commandType: CommandType.Text));
         return produtos;
      }
      
      public Task<int> Update(ProdutoModel produto)
      {
         var dbPara = new DynamicParameters();
         dbPara.Add("Id", produto.Id);
         dbPara.Add("Nome", produto.Nome, DbType.String);
         dbPara.Add("Preco", produto.Preco, DbType.Double);
         var updateProduto = Task.FromResult
            (_dapperService.Update<int>("[dbo].[prcInsAltProduto]",
            dbPara, commandType: CommandType.StoredProcedure));
         return updateProduto;
      }

      public Task<List<ProdutoModel>> FetchAll()
      {
         var bookAuthorNames = Task.FromResult(_dapperService.GetAll
             <ProdutoModel>($"select * from [produto] order by Nome; ", null,
             commandType: CommandType.Text));
         return bookAuthorNames;
      }
   }
}