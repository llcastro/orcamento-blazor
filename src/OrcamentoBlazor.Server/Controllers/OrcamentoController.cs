using OrcamentoBlazor.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class OrcamentoController : Controller
  {
    private readonly IOrcamentoServiceDapper _orcamentoService;

    public OrcamentoController(IOrcamentoServiceDapper orcamentoService)
    {
      this._orcamentoService = orcamentoService;
    }

    [Authorize]
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] OrcamentoModel orcamento)
    {
      var res = await this._orcamentoService.Create(orcamento);

      return Ok(new OrcamentoModel { Id = res, Cliente = orcamento.Cliente });
    }

    [Authorize]
    [HttpPost("getbyid")]
    public async Task<IActionResult> GetById([FromBody] ListParams data)
      {
        var res = await this._orcamentoService.GetById(data.id);

        return Ok(res);
      }

    [Authorize]
    [HttpPost("delete")]
    public async Task<IActionResult> Delete([FromBody] ListParams data)
    {
      var res = await this._orcamentoService.Delete(data.id);

      return Ok(res);
    }
    
    [Authorize]
    [HttpPost("count")]
    public async Task<IActionResult> Count([FromBody] ListParams data)
    {
      var res = await this._orcamentoService.Count(data.search);

      return Ok(res);
    }
    
    [Authorize]
    [HttpPost("listAll")]
    public async Task<List<OrcamentoModel>> ListAll([FromBody] ListParams data)
    {
      var res = await this._orcamentoService.ListAll(data.skip, data.take, data.orderBy, data.direction, data.search);

      return res;
    }

    [Authorize]
    [HttpPost("produto")]
    public async Task<IActionResult> PostProduto([FromBody] OrcamentoProdutoModel orcamento)
    {
      var res = await this._orcamentoService.CreateProduto(orcamento);

      return Ok(new OrcamentoProdutoModel { Id = res, Orcamento = orcamento.Orcamento, Produto = orcamento.Produto });
    }

    [Authorize]
    [HttpPost("deleteProduto")]
    public async Task<IActionResult> DeleteProduto([FromBody] ListParams data)
    {
      var res = await this._orcamentoService.DeleteProduto(data.id);

      return Ok(res);
    }

    [Authorize]
    [HttpPost("fetchAllProdutos")]
    public async Task<List<OrcamentoProdutoModel>> FetchAllProdutos([FromBody] ListParams data)
    {
      var res = await this._orcamentoService.FetchAllProdutos(data.id);

      return res;
    }
  }
}
