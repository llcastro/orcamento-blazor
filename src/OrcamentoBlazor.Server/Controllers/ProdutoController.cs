using OrcamentoBlazor.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ProdutoController : Controller
  {
    private readonly IProdutoServiceDapper _produtoService;

    public ProdutoController(IProdutoServiceDapper produtoService)
    {
      this._produtoService = produtoService;
    }

    [Authorize]
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] ProdutoModel produto)
    {
      var res = await this._produtoService.Create(produto);

      return Ok(new ProdutoResult { Id = res, Nome = produto.Nome, Preco = produto.Preco });
    }

    [Authorize]
    [HttpPost("getbyid")]
    public async Task<IActionResult> GetById([FromBody] ListParams data)
      {
        var res = await this._produtoService.GetById(data.id);

        return Ok(res);
      }

    [Authorize]
    [HttpPost("delete")]
    public async Task<IActionResult> Delete([FromBody] ListParams data)
    {
      var res = await this._produtoService.Delete(data.id);

      return Ok(res);
    }
    
    [Authorize]
    [HttpPost("count")]
    public async Task<IActionResult> Count([FromBody] ListParams data)
    {
      var res = await this._produtoService.Count(data.search);

      return Ok(res);
    }
    
    [Authorize]
    [HttpPost("listAll")]
    public async Task<List<ProdutoModel>> ListAll([FromBody] ListParams data)
    {
      var res = await this._produtoService.ListAll(data.skip, data.take, data.orderBy, data.direction, data.search);

      return res;
    }

    [Authorize]
    [HttpPost("fetchAll")]
    public async Task<List<ProdutoModel>> FetchAll()
    {
      var res = await this._produtoService.FetchAll();

      return res;
    }
  }
}
