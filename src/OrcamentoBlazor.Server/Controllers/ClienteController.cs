using OrcamentoBlazor.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrcamentoBlazor.Server.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ClienteController : Controller
  {
    private readonly IClienteServiceDapper _clienteService;

    public ClienteController(IClienteServiceDapper clienteService)
    {
      this._clienteService = clienteService;
    }

    [Authorize]
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] ClienteModel cliente)
    {
      var res = await this._clienteService.Create(cliente);

      return Ok(new ClienteModel { Id = res, Nome = cliente.Nome, Telefone = cliente.Telefone });
    }

    [Authorize]
    [HttpPost("getbyid")]
    public async Task<IActionResult> GetById([FromBody] ListParams data)
      {
        var res = await this._clienteService.GetById(data.id);

        return Ok(res);
      }

    [Authorize]
    [HttpPost("delete")]
    public async Task<IActionResult> Delete([FromBody] ListParams data)
    {
      var res = await this._clienteService.Delete(data.id);

      return Ok(res);
    }
    
    [Authorize]
    [HttpPost("count")]
    public async Task<IActionResult> Count([FromBody] ListParams data)
    {
      var res = await this._clienteService.Count(data.search);

      return Ok(res);
    }
    
    [Authorize]
    [HttpPost("listAll")]
    public async Task<List<ClienteModel>> ListAll([FromBody] ListParams data)
    {
      var res = await this._clienteService.ListAll(data.skip, data.take, data.orderBy, data.direction, data.search);

      return res;
    }

    [Authorize]
    [HttpPost("fetchAll")]
    public async Task<List<ClienteModel>> FetchAll()
    {
      var res = await this._clienteService.FetchAll();

      return res;
    }
  }
}
