using System.ComponentModel.DataAnnotations;

namespace OrcamentoBlazor.Shared
{
    public class OrcamentoProdutoModel
    {
        public int Id { get; set; }
        [Required]
        public double Orcamento { get; set; }
        [Required]
        public double Produto { get; set; }
        public string ProdutoNome { get; set; }
    }
}