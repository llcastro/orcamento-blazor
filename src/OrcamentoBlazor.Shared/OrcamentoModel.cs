using System.ComponentModel.DataAnnotations;

namespace OrcamentoBlazor.Shared
{
    public class OrcamentoModel
    {
        public int Id { get; set; }
        [Required]
        public double Cliente { get; set; }
        public string ClienteNome { get; set; }
        public string ProdutosList { get; set; }
    }
}