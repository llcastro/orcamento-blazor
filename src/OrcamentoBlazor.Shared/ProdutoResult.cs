using System.ComponentModel.DataAnnotations;

namespace OrcamentoBlazor.Shared
{
    public class ProdutoResult
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public double Preco { get; set; }
    }
}