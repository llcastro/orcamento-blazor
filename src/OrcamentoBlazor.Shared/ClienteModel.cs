using System.ComponentModel.DataAnnotations;

namespace OrcamentoBlazor.Shared
{
    public class ClienteModel
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Telefone { get; set; }
    }
}