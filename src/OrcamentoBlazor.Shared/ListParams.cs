using System.ComponentModel.DataAnnotations;

namespace OrcamentoBlazor.Shared
{
    public class ListParams
    {
        public int id { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
        public string orderBy { get; set; }
        public string direction { get; set; }
        public string search { get; set; }
    }
}