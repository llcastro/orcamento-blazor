using System.ComponentModel.DataAnnotations;

namespace OrcamentoBlazor.Shared
{
    public class ProdutoModel
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public double Preco { get; set; }
    }
}