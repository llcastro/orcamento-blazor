create or alter procedure dbo.prcInsAltOrcamento
    @Id integer = null,
    @Cliente integer
as
begin
   if @Id is null
   begin
      insert into dbo.orcamento(Cliente)
        values (@Cliente);
      set @Id = SCOPE_IDENTITY();
   end
   else
   begin
      update dbo.orcamento
         set Cliente = @Cliente
       where Id = @Id;
   end;

   select @Id;
end
go
