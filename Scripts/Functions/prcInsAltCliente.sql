create or alter procedure dbo.prcInsAltCliente
    @Id integer = null,
    @Nome nvarchar(100),
    @Telefone nvarchar(20)
as
begin
   if @Id is null
   begin
      insert into dbo.cliente(Nome, Telefone)
        values (@Nome, @Telefone);
      set @Id = SCOPE_IDENTITY();
   end
   else
   begin
      update dbo.cliente
         set Nome = @Nome
           , Telefone = @Telefone
       where Id = @Id;
   end;

   select @Id;
end
go
