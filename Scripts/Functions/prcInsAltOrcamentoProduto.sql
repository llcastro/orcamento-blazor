create or alter procedure dbo.prcInsAltOrcamentoProduto
    @Id integer = null,
    @Orcamento integer,
    @Produto integer
as
begin
   if @Id is null
   begin
      insert into dbo.orcamento_produtos(Orcamento, Produto)
        values (@Orcamento, @Produto);
      set @Id = SCOPE_IDENTITY();
   end
   else
   begin
      update dbo.orcamento_produtos
         set Orcamento = @Orcamento
           , Produto = @Produto
       where Id = @Id;
   end;

   select @Id;
end
go
