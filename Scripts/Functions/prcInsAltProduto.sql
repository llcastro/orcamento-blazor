create or alter procedure dbo.prcInsAltProduto
    @Id integer = null,
    @Nome nvarchar(100),
    @Preco float
as
begin
   if @Id is null
   begin
      insert into dbo.produto(Nome, Preco)
        values (@Nome, @Preco);
      set @Id = SCOPE_IDENTITY();
   end
   else
   begin
      update dbo.produto
         set Nome = @Nome
           , Preco = @Preco
       where Id = @Id;
   end;

   select @Id;
end
go
