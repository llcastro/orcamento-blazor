create table dbo.produto(
    Id integer identity(1, 1) not null primary key,
    Nome nvarchar(100) not null,
    Preco float not null
);

create table dbo.cliente(
    Id integer identity(1, 1) not null primary key,
    Nome nvarchar(100) not null,
    Telefone nvarchar(20) not null
);

create table dbo.orcamento(
    Id integer identity(1, 1) not null primary key,
    Cliente integer not null foreign key (Cliente) references cliente (Id)
);

create table dbo.orcamento_produtos(
    Id integer identity(1, 1) not null primary key,
    Orcamento integer not null foreign key (Orcamento) references orcamento (Id) on delete cascade,
    Produto integer not null foreign key (Produto) references produto (Id) on delete cascade
);

